<?php

namespace App\Http\Controllers;

use App\Models\TodoList;
use Carbon\Carbon;
use Illuminate\Support\Facades\{
    Auth,
    Redirect,
    Request,
};
use Inertia\Inertia;

class TodoListController extends Controller
{
    public function index()
    {
        return Inertia::render('TodoLists/Index', [
            'filters' => Request::all('search', 'trashed'),
            'todo_lists' => Auth::user()->todo_lists()
                ->orderBy('created_at', 'desc')
                ->filter(Request::only('search'))
                ->paginate(10)
                ->withQueryString()
                ->through(fn ($todo_list) => [
                    'id' => $todo_list->id,
                    'title' => $todo_list->title,
                    'description' => $todo_list->description,
                    'created_at' => Carbon::parse($todo_list->created_at)->format('d F Y'),
                ]),
        ]);
    }

    public function create()
    {
        return Inertia::render('TodoLists/Create');
    }

    public function store()
    {
        Auth::user()->todo_lists()->create(
            Request::validate([
                'title' => ['required', 'max:255'],
                'description' => ['nullable'],
                'user_id' => ['required'],
            ])
        );

        return Redirect::route('todo-lists')->with('success', 'Todo lists created.');
    }

    public function edit($id)
    {
        $todo_list = TodoList::find($id);
        return Inertia::render('TodoLists/Edit', [
            'todo_list' => [
                'id' => $todo_list->id,
                'title' => $todo_list->title,
                'description' => $todo_list->description,
                'user_id' => $todo_list->user_id,
            ],
        ]);
    }

    public function update(TodoList $todo_list)
    {
        $todo_list->update(
            Request::validate([
                'title' => ['required', 'max:255'],
                'description' => ['nullable'],
                'user_id' => ['required'],
            ])
        );

        return Redirect::route('todo-lists')->with('success', 'Todo lists updated.');
    }

    public function destroy(TodoList $todo_list)
    {
        $todo_list->delete();

        return Redirect::route('todo-lists')->with('success', 'Todo lists deleted.');
    }
}
